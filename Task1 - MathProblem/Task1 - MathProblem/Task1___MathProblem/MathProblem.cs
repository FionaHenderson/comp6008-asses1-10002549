﻿
namespace Task1___MathProblem
{
    public class MathProblem
    {
        public int a, b, i, Sum = 0;

        public int userNo
        {
            get { return userNo; }
            set { userNo = value; }
        }

        public void GetMultiples()
        {
            for (int i = 0; i < userNo; i++)
            {
                a = i % 3;
                b = i % 5;

                if (a == 0 || b == 0)
                {
                    Sum = Sum + i;
                }
            }
        }      
    }
}
