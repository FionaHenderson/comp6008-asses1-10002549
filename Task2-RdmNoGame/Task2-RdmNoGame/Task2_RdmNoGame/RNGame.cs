﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2_RdmNoGame
{
    class RNGame
    {
        static void RandomNumber(List<int> list)
        {            
            string input;
            int userGuess = 0;
            int score = 0;

            for (int i = 0; i < 5; i++)
            {
                Random random = new Random();
                int numberToGuess = random.Next(1, 6);
                while (true)
                {
                    Console.Write("\nI'm thinking of a number between 1 and 5. What is your guess: ");
                    input = Console.ReadLine();
                    bool isNum = int.TryParse(input, out userGuess);

                    if (!isNum)
                    {
                        Console.WriteLine("Invalid Input! Please Try Again!");
                        input = string.Empty;
                    }
                    else
                    {
                        if (numberToGuess == userGuess)
                        {
                            score += 1;       //score = score + 1;
                        }
                        break;
                    }
                }
            }

            Console.WriteLine("\nYour score is " + score);
            list.Add(score);

            RePlay(list);
        }

        static void RePlay(List<int> list)
        {

            Console.WriteLine("\nYour previous score: ");

            foreach (var item in list)
            {
                Console.WriteLine(item);
            }

            while (true)
            {
                Console.WriteLine("\nDo you want to play again? (Y/N): ");
                string input = Console.ReadLine().ToUpper();

                if (input == "Y")
                {
                    RandomNumber(list);
                    break;
                }

                if (input == "N")
                {              //back to the main menu
                    break;
                }

                else if (input != "Y" && input != "N")
                {
                    Console.WriteLine("Invalid Input! Please Try Again!");
                    input = string.Empty;

                }
            }

        }
    }
}

